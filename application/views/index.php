<!doctype html>
<html lang="en">
  <head>
    <title><?= $title; ?> | Customs Center Stiesia</title>
    <link rel="icon" href="<?php echo base_url() ?>assets/img/logo_stiesia.png">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300, 400, 700" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap.css') ?>" >

    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/animate.css') ?>" >

    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/owl.carousel.min.css') ?>" >

    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap-datepicker.css') ?>" >

    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/jquery.timepicker.css') ?>" >

    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/fonts/ionicons/css/ionicons.min.css') ?>" >

     <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/fonts/fontawesome/css/font-awesome.min.css') ?>" >

    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/fonts/flaticon/font/flaticon.css') ?>" >
    
    <!-- table -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/perfect-scrollbar.css') ?>" >
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/main.css') ?>" >
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/util.css') ?>" >

    <!-- Theme Style -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/style.css') ?>" >
  </head>
  <body>
    
    <header role="banner">
      <div class="top-bar">
        <div class="container">
          <div class="row">
            <div class="col-md-6 col-sm-6 col-5">
              <ul class="social list-unstyled">
                <li><a href="#"><span class="fa fa-facebook"></span></a></li>
                <li><a href="#"><span class="fa fa-twitter"></span></a></li>
                <li><a href="https://www.instagram.com/ccstiesia/" target="_blank"><span class="fa fa-instagram"></span></a></li>
              </ul>
            </div>
            <!-- <div class="col-md-6 col-sm-6 col-7 text-right">
              <p class="mb-0">
                <a href="#" class="cta-btn" data-toggle="modal" data-target="#modalAppointment">Make an Appointment</a></p>
            </div> -->
          </div>
        </div>
      </div>

      <!-- <?php echo base_url('assets/img/bea.png') ?> -->

      <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container">
          <a class="navbar-brand" href="index.html"><span><img src="<?php echo base_url('assets/img/logo_stiesia.png') ?>" style="width: 50px">&nbsp<img src="<?php echo base_url('assets/img/bea.png') ?>
" style="width: 50px"></span>  </a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample05" aria-controls="navbarsExample05" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>

          <div class="collapse navbar-collapse" id="navbarsExample05">
            <ul class="navbar-nav ml-auto">
              <li class="nav-item">
                <a class="nav-link active" href="<?php echo base_url('index.php/home') ?>">Home</a>
              </li>

              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="services.html" id="dropdown04" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">About</a>
                <div class="dropdown-menu" aria-labelledby="dropdown04">
                  <a class="dropdown-item" href="<?php echo base_url('index.php/profil') ?>">Profil</a>
                  <a class="dropdown-item" href="<?php echo base_url('index.php/visimisi') ?>">Visi, Misi</a>
                  <a class="dropdown-item" href="<?php echo base_url('index.php/pelayan') ?>">Pelayanan</a>
                </div>

              </li>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="doctors.html" id="dropdown05" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Download</a>
                <div class="dropdown-menu" aria-labelledby="dropdown05">
                  <a class="dropdown-item" href="<?php echo base_url('index.php/ecatalog') ?>">e-catalogue</a>
                </div>
              </li>

              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="doctors.html" id="dropdown05" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Materi</a>
                <div class="dropdown-menu" aria-labelledby="dropdown05">
                  <a class="dropdown-item" href="<?php echo base_url('index.php/materi') ?>">kepabeanan dan cukai</a>
                </div>
              </li>

               <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="doctors.html" id="dropdown05" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Civitas Akademika</a>
                <div class="dropdown-menu" aria-labelledby="dropdown05">
                  <a class="dropdown-item" href="<?php echo base_url('index.php/mahasiswa') ?>">Data Mahasiswa</a>
                  <a class="dropdown-item" href="<?php echo base_url('index.php/dosen') ?>">Data Dosen</a>
                </div>
              </li>

              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="doctors.html" id="dropdown05" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Agenda</a>
                <div class="dropdown-menu" aria-labelledby="dropdown05">
                  <a class="dropdown-item" href="<?php echo base_url('index.php/roadshow') ?>">Roadshow</a>
                  <a class="dropdown-item" href="<?php echo base_url('index.php/softopen') ?>">Soft Opening</a>
                </div>
              </li>

            </ul>
          </div>
        </div>
      </nav>
    </header>
    <!-- END header -->

    <!-- PAGE -->
    <?php $this->load->view($page);?>

    <!-- Footer -->

<!--     <a href="#" class="cta-link element-animate" data-animate-effect="fadeIn" data-toggle="modal" data-target="#modalAppointment">
      <span class="sub-heading">Ready to Visit?</span>
      <span class="heading">Make an Appointment</span>
    </a> -->
    <!-- END cta-link -->

    <footer class="site-footer" role="contentinfo">
      <div class="container">
        <div class="row mb-5 element-animate">
          <div class="col-md-3 mb-5">
            <h3>About</h3>
            <ul class="footer-link list-unstyled">
              <li><a href="<?php echo base_url('index.php/profil') ?>">Profil</a></li>
              <li><a href="<?php echo base_url('index.php/visimisi') ?>">Visi, Misi</a></li>
              <li><a href="<?php echo base_url('index.php/pelayan') ?>">Pelayanan</a></li>
            </ul>
          </div>
          <div class="col-md-3 mb-5">
            <h3>Civitas Akademika</h3>
            <ul class="footer-link list-unstyled">
              <li><a href="<?php echo base_url('index.php/mahasiswa') ?>">Data Mahasiswa</a></li>
              <li><a href="<?php echo base_url('index.php/dosen') ?>">Data Dosen</a></li>
            </ul>
          </div>
          <div class="col-md-3 mb-5">
            <h3>Agenda</h3>
            <ul class="footer-link list-unstyled">
              <li><a href="<?php echo base_url('index.php/roadshow') ?>">Roadshow</a></li>
              <li><a href="<?php echo base_url('index.php/softopen') ?>">Soft Opening</a></li>
            </ul>
          </div>
          <div class="col-md-3 mb-5">
            <h3>Location &amp; Contact</h3>
            <p class="mb-5">Jalan Menur Pumpungan 30 Surabaya - 60118</p>

            <h4 class="text-uppercase mb-3 h6 text-white">Email</h5>
            <p class="mb-5"><a href="mailto:info@yourdomain.com">ccstiesia@gmail.com</a></p>
            
            <h4 class="text-uppercase mb-3 h6 text-white">Phone</h5>
            <p>031-594 7505 ext. 329</p>

          </div>
        </div>

        <div class="row pt-md-3 element-animate">
          <div class="col-md-12">
            <hr class="border-t">
          </div>
          <div class="col-md-6 col-sm-12 copyright">
            <p>&copy; 2018 Customs Center Stiesia. Designed &amp; Developed by <a href="https://colorlib.com/">Puskom Stiesia Surabaya</a></p>
          </div>
          <div class="col-md-6 col-sm-12 text-md-right text-sm-left">
            <a href="#" class="p-2"><span class="fa fa-facebook"></span></a>
            <a href="#" class="p-2"><span class="fa fa-twitter"></span></a>
            <a href="https://www.instagram.com/ccstiesia/" target="_blank" class="p-2"><span class="fa fa-instagram"></span></a>
          </div>
        </div>
      </div>
    </footer>
    <!-- END footer -->

    <a href="#" class="back-to-top">
    <i class="icon-arrow-up"></i>
    </a>


    <script src="<?php echo base_url('assets/js/jquery-3.2.1.min.js') ?>"></script>

     <script src="<?php echo base_url('assets/js/popper.min.js') ?>"></script>

   <script src="<?php echo base_url('assets/js/bootstrap.min.js') ?>"></script>

    <script src="<?php echo base_url('assets/js/owl.carousel.min.js') ?> "></script>

    <script src="<?php echo base_url('assets/js/bootstrap-datepicker.js') ?>"></script>

    <script src="<?php echo base_url('assets/js/jquery.timepicker.min.js') ?>"></script>

    <script src="<?php echo base_url('assets/js/jquery.waypoints.min.js') ?>"></script>

    <script src="<?php echo base_url('assets/js/main.js') ?>"></script>

    <script src="<?php echo base_url('assets/js/select2.min.js') ?>"></script>

  </body>
</html>