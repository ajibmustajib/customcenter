
 <section class="section stretch-section">
      <div class="container">
        <div class="row justify-content-center mb-5 element-animate">
          <div class="col-md-8 text-center mb-5">
            <h2 class="text-uppercase heading border-bottom mb-4">Profil Customs Center</h2>
          </div>
        </div>

        <div class="row align-items-center" style="margin-top: -5%">
          <div class="col-md-12 stretch-left-1-offset pl-md-5 pl-0 element-animate" data-animate-effect="fadeInLeft">
              <p style="text-align: justify">STIESIA Surabaya merupakan satu-satunya kampus di Indonesia yang mendirikan Customss Center. Keberadaan Customss Center tersebut sebagai wadah bagi masyarakat dan mahasiswa agar dapat merealisasikan bidang ilmu kepabeanan.<br>
Pelatihan yang diterima selama proses perkuliahan tersebut dapat memberikan layanan kepada pengguna jasa berupa informasi dan edukasi terkait proses kepabeanan. Edukasi yang diberikan antara lain tata cara impor dan ekspor, pengisian dokumen impor dan ekspor, hingga informasi terbaru mengenai kepabeanan.</p>
          </div>
        </div>
      </div>

    </section>