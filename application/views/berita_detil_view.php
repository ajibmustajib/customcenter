 <section class="section stretch-section">
      <div class="container">
        
        <div class="row align-items-center" style="margin-top: -5%">
          <div class="col-md-12 stretch-left-1-offset pl-md-5 pl-0 element-animate" data-animate-effect="fadeInLeft">
            <h2 class="text-primary">Berita Satu</h2>
              <p style="text-align: justify">
                
                Ribuan butir ekstasi tersebut dikirim melalui paket Kantor Pos MPC Bandung 40400 asal Bangladesh yang ditujukan kepada seorang F beralamat di Kota Bandung.


"Petugas Bea Cukai melakukan analisa terhadap barang kiriman tersebut, ditemukan anomali terhadap X- ray dan pemeriksaan menggunakan Unit K9 Kanwil Bea Cukai Jawa Barat," kata Kepala Kantor Wilayah Dirjen Bea dan Cukai Jawa Barat, Saefullah Nasution, di Kantor Bea Cukai Bandung, Senin (7/5/2018).

Pil ekstasi tersebut disembunyikan di dalam mainan dan langsung dilakukan uji laboratorium dan positif masuk narkotika golongan satu.

Barang bukti tersebut langsung diserahkan kepada Sat Res Narkoba Polrestabes Bandung untuk diproses lebih lanjut.



Dari tablet yang disita, terdapat dua jenis warna, yaitu warna merah dan abu-abu. Mobil mainan tersebut menyerupai mobil polisi.

Kerugian material dan immaterial yang akan timbul jika barang haram tersebut beredar ialah, uang tunai sebesar Rp 781.200.000, dan telah menyelamatkan sebanyak 3.906 jiwa.

Akibat perbuatannya, F akan diancam pasal 113 ayat 2 UU No 35 tahun 2009 tentang narkotika dengan ancaman hukuman berupa pidana mati, pidana penjara seumur hidup, atau pidana penjara paling singkat lima tahun, dan paling lama 20 tahun serta denda paling banyak sebesar 3/4 dari Rp 10 miliar.

Penggagalan penyelundupan Narkotika, Psikotropika dan Prekusor (NPP) merupakan yang ke-10 dalam tahun 2018 yang dilakukan Bea Cukai Bandung.



Artikel ini telah tayang di Tribunnews.com dengan judul Penyelundupan 1.953 Butir Ekstasi dari Bangladesh Digagalkan, http://www.tribunnews.com/regional/2018/05/07/penyelundupan-1953-butir-ekstasi-dari-bangladesh-digagalkan.

Editor: Eko Sutriyanto
              </p>
          </div>
        </div>
      </div>

    </section>