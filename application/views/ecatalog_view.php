 <section class="section stretch-section">
      <div class="container">
        <div class="row justify-content-center mb-5 element-animate">
          <div class="col-md-8 text-center mb-5">
            <h2 class="text-uppercase heading border-bottom mb-4">E-Catalogue Customs Center</h2>
          </div>
        </div>

        <div class="row align-items-center" style="margin-top: -5%">
          <div class="col-md-12 stretch-left-1-offset pl-md-5 pl-0 element-animate" data-animate-effect="fadeInLeft">
              <ul>
                <li>e-catalogue 1 &nbsp <a href="#">Download</a></li>
                <li>e-catalogue 2 &nbsp <a href="#">Download</a></li>
                <li>e-catalogue 3 &nbsp <a href="#">Download</a></li>
              </ul>
          </div>
        </div>
      </div>

    </section>