 <section class="section stretch-section">
      <div class="container">
        <div class="row justify-content-center mb-5 element-animate">
          <div class="col-md-8 text-center mb-5">
            <h2 class="text-uppercase heading border-bottom mb-4">Data Dosen Customs Center</h2>
          </div>
        </div>

        <div class="row align-items-center" style="margin-top: -5%">

          <div class="col-md-12 stretch-left-1-offset pl-md-5 pl-0 element-animate" data-animate-effect="fadeInLeft"">

                <div class="table100 ver4 m-b-110">
                      <div class="table100-head">
                        <table>
                          <thead>
                            <tr class="row100 head">
                            
                              <th class="cell100 column2">NIDN</th>
                              <th class="cell100 column3">Nama</th>
                              <th class="cell100 column4">Alamat</th>
                              <th class="cell100 column5">Email</th>
                            </tr>
                          </thead>
                        </table>
                      </div>

                <div class="table100-body js-pscroll">
                  <table>
                    <tbody>
                      <tr class="row100 body">
                       
                        <td class="cell100 column2">452345</td>
                        <td class="cell100 column3">Arif Reza Ahmad M.Eng</td>
                        <td class="cell100 column4">Jalan Sidoarjo</td>
                        <td class="cell100 column5">reza@gmail.com</td>
                      </tr>

                      <tr class="row100 body">
                        
                        <td class="cell100 column2">314235</td>
                        <td class="cell100 column3">Ahmad Jono S.Ag</td>
                        <td class="cell100 column4">Jalan Guci</td>
                        <td class="cell100 column5">jono@gmail.com</td>
                      </tr>

                      <tr class="row100 body">
                        
                        <td class="cell100 column2">243523</td>
                        <td class="cell100 column3">Listya Ayu Wardana M.Kom</td>
                        <td class="cell100 column4">Jalan Stikom</td>
                        <td class="cell100 column5">listya@gmail.com</td>
                      </tr>

                      <tr class="row100 body">
                        
                        <td class="cell100 column2">457455</td>
                        <td class="cell100 column3">Ahmad Hafidz</td>
                        <td class="cell100 column4">Jalan SCC</td>
                        <td class="cell100 column5">hafidz@gmail.com</td>
                      </tr>
     
                    </tbody>
                  </table>
                </div>
              </div>


          </div>

        </div>
      </div>

    </section>