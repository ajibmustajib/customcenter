    <!-- slider -->

    <section class="home-slider owl-carousel">
      <div class="slider-item" style="background-image: url('<?php echo base_url() ?>assets/img/cc1.jpg');">
        
        <div class="container">
          <div class="row slider-text align-items-center">
            <div class="col-md-7 col-sm-12 element-animate">
              <h1 style="font-weight: 750" >Customs Center Stiesia</h1>
              <p>Layanan prima kami, iringi kesuksesan anda.</p>
            </div>
          </div>
        </div>

      </div>

      <div class="slider-item" style="background-image: url('<?php echo base_url() ?>assets/img/cc2.jpg');">
        <div class="container">
          <div class="row slider-text align-items-center">
            <div class="col-md-7 col-sm-12 element-animate">
              <h1 style="font-weight: 750" >Customs Center Stiesia</h1>
              <p>Layanan prima kami, iringi kesuksesan anda.</p>
            </div>
          </div>
        </div>
        
      </div>
    </section>
    <!-- end slider -->

    <!-- Agenda -->

    <section class="section custom-tabs bg-light">
      <div class="container">

        <div class="row justify-content-center mb-5 element-animate">
          <div class="col-md-8 text-center mb-5">
            <h2 class="text-uppercase heading border-bottom mb-4">Berita Terbaru</h2>
          </div>
        </div>

        <div class="row">

          <div class="col-md-4 border-right element-animate">
            <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
              <a class="nav-link active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true"><span>01</span> berita satu</a>
              <a class="nav-link" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false"><span>02</span> berita dua</a>
              <a class="nav-link" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab" aria-controls="v-pills-messages" aria-selected="false"><span>03</span> berita tiga</a>
              <a class="nav-link" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings" role="tab" aria-controls="v-pills-settings" aria-selected="false"><span>04</span> berita empat</a>
            </div>
          </div>
          <div class="col-md-1"></div>
          <div class="col-md-7 element-animate">
            
            <div class="tab-content" id="v-pills-tabContent">
              <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                <!-- <span class="icon flaticon-hospital"></span> -->
                <h2 class="text-primary">Berita Satu</h2>
                <p class="lead">"Petugas Bea Cukai melakukan analisa terhadap barang kiriman tersebut, ditemukan anomali terhadap X- ray dan pemeriksaan menggunakan Unit K9 Kanwil Bea Cukai Jawa Barat," kata Kepala Kantor Wilayah Dirjen Bea dan Cukai Jawa Barat, Saefullah Nasution, di Kantor Bea Cukai Bandung, Senin (7/5/2018).</p>
                <p><a href="http://localhost/customcenter/index.php/berita_detil" class="btn btn-primary">Baca Selengkapnya</a></p>
              </div>
              <div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
                <!-- <span class="icon flaticon-first-aid-kit"></span> -->
                <h2 class="text-primary">Berita Dua</h2>
                <p class="lead">Direktorat Jenderal Bea dan Cukai Kementerian Keuangan (Ditjen Bea Cukai)melaksanakan permintaan Kementerian Koordinator Perekonomian (Kemenko Perekonomian) untuk menunda pembatasan impor tembakau.</p>
            
                <p><a href="http://localhost/customcenter/index.php/berita_detil" class="btn btn-primary">Baca Selengkapnya</a></p>
              </div>
              <div class="tab-pane fade" id="v-pills-messages" role="tabpanel" aria-labelledby="v-pills-messages-tab">
                <!-- <span class="icon flaticon-hospital-bed"></span> -->
                <h2 class="text-primary">Berita Tiga</h2>
                <p class="lead">Polisi Resort (Polsek) Mutiara Timur, Pidie kini menangani kasus dugaan penipuan yang telah dilaporkan warga Kecamatan Mutiara Timur dan Tangse.</p>
                <p><a href="http://localhost/customcenter/index.php/berita_detil" class="btn btn-primary">Baca Selengkapnya</a></p>
              </div>
              <div class="tab-pane fade" id="v-pills-settings" role="tabpanel" aria-labelledby="v-pills-settings-tab">
                <!-- <span class="icon flaticon-doctor"></span> -->
                <h2 class="text-primary">Berita Empat</h2>
                <p class="lead">"24 tengkorak manusia itu dikirim ke Belanda dan dihentikan oleh Bea Cukai Ngurah Rai. Mereka dihentikan dalam dua kasus oleh Kantor Pos, " kata Kepala Kantor Bea Cukai Ngurah Rai, Himawan Indarjono, di Bali, dilansir dari Coconuts News.</p>
                <p><a href="http://localhost/customcenter/index.php/berita_detil" class="btn btn-primary">Baca Selengkapnya</a></p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- END section -->


    <!-- Sambutan Ketua Stiesia -->

    <section class="section stretch-section">
      <div class="container">
        <div class="row justify-content-center mb-5 element-animate">
          <div class="col-md-8 text-center mb-5">
            <h2 class="text-uppercase heading border-bottom mb-4">Sambutan Ketua Stiesia</h2>
          </div>
        </div>

        <div class="row align-items-center">
          
          <div class="col-md-3 stretch-left-1 element-animate" data-animate-effect="fadeInLeft">
            <img src="<?php echo base_url() ?>assets/img/ketua_stiesia.gif" style="width:90%" alt="" class="img-fluid">
          </div>
          <div class="col-md-9 stretch-left-1-offset pl-md-5 pl-0 element-animate" data-animate-effect="fadeInLeft">
              <p>Pada tanggal 20 April 2017, Sekolah Tinggi Ilmu Ekonomi Indonesia (STIESIA) genap berusia 45 tahun. Perkembangan, capaian, dan kontribusi STIESIA kepada masyarakat hingga pada usia tersebut akan terus kami tingkatkan di masa depan. Fakta menunjukkan, bahwa kunci keberhasilan STIESIA dalam melaksanakan program-program pendidikan tidak hanya terletak pada dedikasi dosen dan karyawan (tenaga kependidikan), tetapi juga pada semangat mahasiswa untuk menciptakan dan menumbuhkan budaya akademik yang mendukung keberhasilan proses belajar - mengajar.</p>
          </div>
        </div>

        <br>

        <div class="row align-items-center">

          <div class="col-md-12 stretch-left-1 element-animate" data-animate-effect="fadeInLeft">
              <p><b>Dr. Nur Fadjrih Asyik, S.E., M.Si., Ak., CA</b></p>
          </div>

        </div>

    </section>
    <!-- END section -->

    <!-- our partner -->
    <section class="section Customss-tabs bg-light">
      <div class="container">

          <div class="row justify-content-center mb-5 element-animate">
          <div class="col-md-8 text-center mb-5">
            <h2 class="text-uppercase heading border-bottom mb-4">Apa Kata Mereka</h2>
          </div>
        </div>

        
      </div>
    </section>
    