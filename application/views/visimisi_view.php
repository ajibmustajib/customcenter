 <section class="section stretch-section">
      <div class="container">
        <div class="row justify-content-center mb-5 element-animate">
          <div class="col-md-8 text-center mb-5">
            <h2 class="text-uppercase heading border-bottom mb-4">Visi Misi Customs Center</h2>
          </div>
        </div>

        <div class="row align-items-center" style="margin-top: -5%">
          <div class="col-md-12 stretch-left-1-offset pl-md-5 pl-0 element-animate" data-animate-effect="fadeInLeft">
            <h1>Visi</h1>
              <p style="text-align: justify">Mewujudkan Customs Center Stiesia Menjadi yang Terbaik Se-indonesia tahun 2019</p>
          </div>
        </div>
        
        <br>
        <br>
        <br>

        <div class="row align-items-center" style="margin-top: -5%">
          <div class="col-md-12 stretch-left-1-offset pl-md-5 pl-0 element-animate" data-animate-effect="fadeInLeft">
            <h1>Misi</h1>
              <ul>
                <li>Semangat</li>
                <li>Giat</li>
                <li>Disiplin</li>
                <li>Toleransi</li>
              </ul>
          </div>
        </div>
      </div>

    </section>