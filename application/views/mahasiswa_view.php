 <section class="section stretch-section">
      <div class="container">
        <div class="row justify-content-center mb-5 element-animate">
          <div class="col-md-8 text-center mb-5">
            <h2 class="text-uppercase heading border-bottom mb-4">Data Mahasiswa Customs Center</h2>
          </div>
        </div>

        <div class="row align-items-center" style="margin-top: -5%">

          <div class="col-md-12 stretch-left-1-offset pl-md-5 pl-0 element-animate" data-animate-effect="fadeInLeft"">

                <div class="table100 ver4 m-b-110">
                      <div class="table100-head">
                        <table>
                          <thead>
                            <tr class="row100 head">
                            
                              <th class="cell100 column2">Nama</th>
                              <th class="cell100 column3">Alamat</th>
                              <th class="cell100 column4">Program Studi</th>
                              <th class="cell100 column5">Email</th>
                            </tr>
                          </thead>
                        </table>
                      </div>

                <div class="table100-body js-pscroll">
                  <table>
                    <tbody>
                      <tr class="row100 body">
                       
                        <td class="cell100 column2">Muhammad Jono</td>
                        <td class="cell100 column3">Jalan Njagir Surabaya</td>
                        <td class="cell100 column4">D3 Sistem Informasi</td>
                        <td class="cell100 column5">jono@gmail.com</td>
                      </tr>

                      <tr class="row100 body">
                        
                        <td class="cell100 column2">Yoga Suparjo</td>
                        <td class="cell100 column3">Jalan Soekarno Surabaya</td>
                        <td class="cell100 column4">S1 Sistem Informasi</td>
                        <td class="cell100 column5">jono@gmail.com</td>
                      </tr>

                      <tr class="row100 body">
                        
                        <td class="cell100 column2">Muhammad Joni</td>
                        <td class="cell100 column3">Jalan Romoo Surabaya</td>
                        <td class="cell100 column4">D3 Sistem Informasi</td>
                        <td class="cell100 column5">jono@gmail.com</td>
                      </tr>

                      <tr class="row100 body">
                        
                        <td class="cell100 column2">Yoga Suparji</td>
                        <td class="cell100 column3">Jalan Hatta Surabaya</td>
                        <td class="cell100 column4">S1 Sistem Informasi</td>
                        <td class="cell100 column5">jono@gmail.com</td>
                      </tr>
     
                    </tbody>
                  </table>
                </div>
              </div>


          </div>

        </div>
      </div>

    </section>