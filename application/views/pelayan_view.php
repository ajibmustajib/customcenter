 <section class="section stretch-section">
      <div class="container">
        <div class="row justify-content-center mb-5 element-animate">
          <div class="col-md-8 text-center mb-5">
            <h2 class="text-uppercase heading border-bottom mb-4">Pelayanan Customs Center</h2>
          </div>
        </div>

        <div class="row align-items-center" style="margin-top: -5%">
          <div class="col-md-12 stretch-left-1-offset pl-md-5 pl-0 element-animate" data-animate-effect="fadeInLeft">
            <ul>
              <li>Pelayanan Pembebasan Bea Masuk atas Impor Bibit dan Benih untuk Pembangunan dan Pengembangan Industri Pertanian, Peternakan dan Perikanan</li>
              <li>Pelayanan Pemberian Keringanan Bea Masuk atas Impor Barang dalam rangka Pembangunan/Pengembangan Industri/Industri Jasa</li>
              <li>Pelayanan Pemesanan Pita Cukai Hasil Tembakau (CK-1) secara Manual</li>
              <li>Pelayanan Penerbitan SK Pembebasan dalam rangka KITE dengan Menggunakan Media Penyimpan Data Elektronik</li>
              <li>Pelayanan Penerbitan Nomor Induk Perusahaan (NIPER) dalam rangka KITE secara Manual</li>
              <li>Pelayanan Penyelesaian Barang Impor untuk Dipakai Jalur MITA Prioritas dengan PIB yang Disampaikan Melalui Sistem PDE Kepabeanan</li>
            </ul>
              
          </div>
        </div>

      </div>

    </section>